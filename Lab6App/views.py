from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import Statusku
from .forms import FormKegiatan
from django.contrib import messages
def Home(request):
    return render(request, 'Lab6.html')
def ProfileS(request):
	return render(request,'About.html')
def sched(request):
	if request.method == "POST":
		form = FormKegiatan(request.POST)
		if form.is_valid():
			Statusku(Status_Kamu=form.cleaned_data['Status_Kamu'],).save()
			print(form.cleaned_data['Status_Kamu'])
		return HttpResponseRedirect('/')
	else:
		form = FormKegiatan()
		dataStatus = Statusku.objects.all()
		context = {
		'Statusnya' : dataStatus,
		}
	return render(request,'Lab6.html',{'form': form,'Statusnya' : dataStatus})
def hapusdata(request):
	Statusku.objects.all().delete()
	
	return HttpResponseRedirect('/')