from django.test import TestCase, Client
from django.urls import resolve
from .views import sched
from .views import ProfileS
from .models import Statusku
from .forms import FormKegiatan
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time



class Story6test(TestCase):

    def test_Story6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_Story6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'Lab6.html')
    	
    def test_Story6_using_Profile_template(self):
        response = Client().get('/Profile/')
        self.assertTemplateUsed(response,'About.html')
    def test_Story6_using_url_is_exist(self):
    	response = Client().get('/')
    	self.assertEqual(response.status_code,200)
    def test_Story6_using_addStatus_func(self):
        found = resolve('/')
        self.assertEqual(found.func,sched)
    def test_Story6_using_Profile_func(self):
        found = resolve('/Profile/')
        self.assertEqual(found.func,ProfileS)
    
    def test_model_can_create_new_status(self):
        new_activity = Statusku.objects.create(Status_Kamu='Isi status')
        counting_all_available_activity = Statusku.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    # def test_can_save_a_POST_request(self):
    #     response = self.client.post('/', data={'judul_Status': 'ini judul', 'isi_Status' : 'aku mau latihan ngoding dehh'})
    #     counting_all_available_activity = Status.objects.all().count()
    #     self.assertEqual(counting_all_available_activity, 1)
    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(response['location'], '/')
    #     new_response = self.client.get('/')
    #     html_response = new_response.content.decode('utf8')
    #     self.assertIn('aku mau latihan ngoding dehh', html_response)

    def test_Story6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'Status_Kamu': test})
        self.assertEqual(response_post.status_code, 302)
    
            #response= Client().get('/')
            #html_response = response.content.decode('utf8')
            #self.assertIn(test, html_response)

    # def test_Story6_post_error_and_render_the_result(self):
    #         test = 'Anonymous'
    #         response_post = Client().post('/', { 'Status_Kamu': ''})
    #         self.assertEqual(response_post.status_code, 302)
    
    #         response= Client().get('/')
    #         html_response = response.content.decode('utf8')
    #         self.assertNotIn(test, html_response)

    # def test_string_representation(self):
    #     string = Status(judul_Status="ngoding kuy")
    #     self.assertEqual(str(string), string.judul_Status)

    def test_create_anything_model(self,  isi='ini isi dtatus'):
    	return Statusku.objects.create( Status_Kamu=isi)

class ppwlabFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(ppwlabFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(ppwlabFunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://ppw-d-webgokil.herokuapp.com/')
        # find the form element
        title = selenium.find_element_by_id('id_Status_Kamu')
  

        submit = selenium.find_element_by_id('id_Submit')
        hapus = selenium.find_element_by_id('id_Hapus')

        # # Fill the form with data	
        title.send_keys('Submit')
        title.send_keys('Hapus')
        
        # # submitting the form
        submit.send_keys(Keys.RETURN)
        
        time.sleep(5)
    def test_layout_title(self):
    	selenium = self.selenium
    	selenium.get('https://ppw-d-webgokil.herokuapp.com/')

    	title_layout = selenium.find_element_by_css_selector('h1')
    	self.assertIn('HALO BOSKU APA KABAR SEMUANYA',self.selenium.page_source )
    def test_label_layout(self):
    	selenium = self.selenium
    	selenium.get('https://ppw-d-webgokil.herokuapp.com/')

    	title_label = selenium.find_element_by_id('id_Status_Kamu')
    	self.assertIn('Statuslo',self.selenium.page_source)




 


