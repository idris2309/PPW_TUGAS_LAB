from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from . import views
from Lab6App import views as Lab6views
    #url for app
urlpatterns = [
    url(r'^admin/',admin.site.urls),
    url(r'^$',Lab6views.sched),
    url(r'^deletejadwal/',Lab6views.hapusdata),
    url(r'^Profile/',Lab6views.ProfileS)
    ]
