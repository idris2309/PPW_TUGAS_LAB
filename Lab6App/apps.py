from django.apps import AppConfig


class Lab6AppConfig(AppConfig):
    name = 'Lab6App'
