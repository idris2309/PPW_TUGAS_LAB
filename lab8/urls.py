from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from . import views
from lab8 import views as lab8views
    #url for app
urlpatterns = [
    url(r'^admin/',admin.site.urls),
    
    url(r'^$',lab8views.Profile)
    ]
