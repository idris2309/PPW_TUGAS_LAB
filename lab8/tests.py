from django.test import TestCase, Client
from django.urls import resolve
from .views import Profile

import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Story8test(TestCase):
		def test_Story8_url_is_exist(self):
				response = Client().get("/")
				self.assertEqual(response.status_code,200)
		def test_Story8_using_index_template(self):
				response = Client().get("/")
				self.assertTemplateUsed(response,'Aboutjss.html')
		def test_Story8_using_addStatus_func(self):
				found = resolve('/')
				self.assertEqual(found.func,Profile)

class test_newvisitor_Story8(unittest.TestCase):
		def setUp(self):
			chrome_options = Options()
			chrome_options.add_argument('--dns-prefetch-disable')
			chrome_options.add_argument('--no-sandbox')
			chrome_options.add_argument('--headless')
			chrome_options.add_argument('disable-gpu')
			service_log_path = "./chromedriver.log"
			service_args = ['--verbose']

			self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
			self.browser.implicitly_wait(3)
			super(test_newvisitor_Story8, self).setUp()

		def tearDown(self):
			self.browser.implicitly_wait(3)
			self.browser.quit()
			super(test_newvisitor_Story8, self).tearDown()

		def test_Terang_and_Dark(self):
			self.browser.get('https://ppw-d-webgokil.herokuapp.com/')
			time.sleep(2)

			self.browser.find_element_by_id("terang").click()
			self.browser.find_element_by_id('gelap').click()

			time.sleep(2)
		def test_css(self):
			self.browser.get('https://ppw-d-webgokil.herokuapp.com/')
			time.sleep(1)

			Terang_button = self.browser.find_element_by_id('terang')
			Dark_button = self.browser.find_element_by_id('gelap')

			self.assertIn("Terang",Terang_button.text)
			self.assertIn("Dark",Dark_button.text)

			Aktivitas = self.browser.find_element_by_id('ak')
			Pengalaman = self.browser.find_element_by_id('peng')
			Prestasi = self.browser.find_element_by_id('pres')

			Aktivitas_use_class = 'accordion' in Aktivitas.get_attribute("class")
			Pengalaman_use_class = 'accordion' in Pengalaman.get_attribute("class")
			Prestasi_use_class = 'accordion' in Prestasi.get_attribute("class")

			self.assertTrue(Aktivitas_use_class)
			self.assertTrue(Pengalaman_use_class)
			self.assertTrue(Prestasi_use_class)

if __name__ == '__main__':
	unittest.main(warnings = 'ignore')
