from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from lab9 import views as lab9views
    #url for app
urlpatterns = [
    url(r'^admin/',admin.site.urls),
    
    url(r'^$',lab9views.Buku),
    url(r'^api/books/(?P<search>\w+)/$',lab9views.databookos)
    ]
