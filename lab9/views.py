from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.http import JsonResponse
from django.contrib import messages
import urllib.request, json
import requests 
def Buku(request):
	return render(request,'Book.html')
def databookos(request,search):
	# data_dari = request.GET.get('judul','quilting')
	data_json = requests.get('https://www.googleapis.com/books/v1/volumes?q='+search).json()
	# items = []
	# for i in data_json['items']:
	# 	info = {}
	# 	info['title'] = i['volumeInfo']['title']
	# 	info['publishedDate'] = i['volumeInfo']['publishedDate']
	# 	info['pageCount'] = i['volumeInfo']['pageCount']
	# 	info['language'] = i['volumeInfo']['language']
	# 	info['categories'] = i['volumeInfo']['categories']
	# 	info['authors'] = ", ".join(i['volumeInfo']['authors'])
	# 	items.append(info)
	return JsonResponse({"info" :data_json})
