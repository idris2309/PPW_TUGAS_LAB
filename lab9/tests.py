from django.test import TestCase, Client
from django.urls import resolve
from .views import Buku
from .views import databookos
# Create your tests here.
class Story9test(TestCase):

    def test_Story9_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_Story9_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'Book.html')
    	
    def test_Story9_using_addStatus_func(self):
        found = resolve('/')
        self.assertEqual(found.func,Buku)
    # def test_Story9_using_databookos_func(self):
    # 	found = resolve('/api/books/')
    # 	self.assertEqual(found.func,databookos)
    # def test_AJAX(self):
    # 	response = Client().get('/api/books')
    # 	self.assertEqual(response.status_code,404)