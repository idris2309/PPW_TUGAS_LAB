from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import Jadwalku
from .forms import FormKegiatan
from django.contrib import messages
def Home(request):
	return render(request,'blank.html')
def Profile(request):
	return render(request,'About.html')
def Form(request):
	return render(request,'Untitled.html')
def Contact(request):
	return render(request,'Contaxk.html')
def Keahlian(request):
	return render(request,'Special.html')
def TimeforU(request):
	return render(request,'JadwalU.html')
def Schedule(request):
	Jadwala = Jadwalku.objects.all()
	context = {
	'Jadwalkus' : Jadwala,
	}
	return render(request,'Jadwal.html',context)
def sched(request):
	if request.method == "POST":
		form = FormKegiatan(request.POST)
		if form.is_valid():
			Jadwalku(judul=form.cleaned_data['judul'],
				tanggal = form.cleaned_data['tanggal'],
				jam = form.cleaned_data['jam'],
				kegiatan = form.cleaned_data['kegiatan'],
				kategori = form.cleaned_data['kategori'],
				tempat = form.cleaned_data['tempat'] ).save()
		return HttpResponseRedirect('/')
	else:
		form = FormKegiatan()
	return render(request,'JadwalU.html',{'form': form})		
def hapusdata(request):
	Jadwalku.objects.all().delete()
	return HttpResponseRedirect('/jadwalku/')