
from django import forms
from Lab_4app.models import Jadwalku
from django.forms import ModelForm
#class FormKegiatan(forms.ModelForm):
#	class Meta:
#		model = Jadwalku
#		fields = ['judul','tanggal','jam','kegiatan', 'kategori','tempat']
#		 widgets = {
#		 	'judul': forms.TextInput(attrs={'class' : 'form-control'}),
#		 	'tanggal': forms.TextInput(attrs={'type':'date','class' : 'form-control'}),
#		 	'jam': forms.TextInput(attrs={'type':'jam', 'class': 'form-control'}),
#		 	'kegiatan': forms.TextInput(attrs={'class': 'form-control'}),
#		 	'kategori': forms.TextInput(attrs={'class': 'form-control'}),
#		 	'tempat': forms.TextInput(attrs={'class': 'form-control'}),
#			
#		 }
		# label = {
		# 	'judul': 'Judul',
		# 	'tanggal': 'Tanggal',
		# 	'jam': 'Time',
		# 	'kegiatan': 'Activity',

		# 	'tempat': 'Place'
		# }
class FormKegiatan(forms.Form):
	attrs = {'class': 'form-control'}
	judul = forms.CharField(label = 'Title', max_length = 20 , widget=forms.TextInput(attrs=attrs) )
	tanggal = forms.DateField(label='date',widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))
	jam = forms.TimeField(label = 'Time',widget=forms.TimeInput(attrs={'class': 'form-control', 'type': 'time'}))
	kegiatan = forms.CharField(label = 'Kegiatan' ,max_length = 30 , widget=forms.TextInput(attrs=attrs))
	kategori =  forms.CharField(label = 'Kategori', max_length = 20 ,widget=forms.TextInput(attrs=attrs))
	tempat = forms.CharField(label = 'tempat',max_length = 50 , widget=forms.TextInput(attrs=attrs))

#class FormKegiatan(forms.Form):
 #   error_messages = {
  #      'required': 'Tolong isi input ini',
   #     'invalid': 'Isi input dengan email',
    #}
    #attrs = {
        #'class': 'form-control'
    #}
    
    #Judul = forms.CharField(label='Judul', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    #tanggal = forms.DateField(label = 'date'required=False, widget=forms.DateInput(attrs=attrs))
    #jam = forms.TimeField(widget=forms.TimeInput(attrs=attrs), required=True)
    #kegiatan = forms.CharField(label = 'Kegiatan' ,max_length = 30 , widget=forms.TextInput(attrs=attrs))
    #	tempat = forms.CharField(label = 'tempat',max_length = 50 , widget=forms.TextInput(attrs=attrs))