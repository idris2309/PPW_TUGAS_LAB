from django.db import models
from django.utils import timezone
from datetime import datetime


# Create your models here.
class Jadwalku (models.Model):
	judul = models.CharField(max_length = 20)
	tanggal = models.DateField()
	jam = models.TimeField()
	kegiatan = models.CharField(max_length = 30)
	kategori =  models.CharField(max_length = 20)
	tempat = models.CharField(max_length = 50)

	def __str__(self):
		return "{}".format(self.judul)