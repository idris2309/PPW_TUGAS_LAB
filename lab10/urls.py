from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from lab10 import views as lab10views
    #url for app
urlpatterns = [
    url(r'^admin/',admin.site.urls),
    url('validate_email',lab10views.validate_email,name = "validate_email"),
    url('Member',lab10views.Member,name = "Member"),
    url('hapusSub', lab10views.unsub,name = "unsub"),
	url('Subscribe',lab10views.register),
    url(r'^$',lab10views.ProfileS)
    ]
