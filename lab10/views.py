from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from .models import Signup
from .forms import Formregister
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import json
import re
def ProfileS(request):
	return render(request,'Aboutjss1.html')
def register(request):
    return render(request, 'Regis.html')
@csrf_exempt
def Member(request):
	if request.method == "POST":
		form = Formregister(request.POST)
		if form.is_valid():
			Signup(nama =form.cleaned_data['nama'],
				email = form.cleaned_data['email'],
				password = form.cleaned_data['password']).save()
		return HttpResponseRedirect('/')
	else:
		form = Formregister()
		dataMember = Signup.objects.all()
		context = {
		'Membernya' : dataMember,
		}
	return render(request,'Regis.html',{'form': form,'Membernya' : dataMember})
@csrf_exempt
def validate_email(request):
	email_validate_check = False
	enctype = "multipart/form-data"
	if request.method == "POST":
		email = request.POST.get('email')


		if(len(email))>=1:
			email_validate_check = bool(re.match("^.+@(\[?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$",email))
		


		data = {'is_taken':Signup.objects.filter(email = email).exists(),'is_email_valid':email_validate_check}
		return JsonResponse(data)
	else:
		return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")


def model_to_dict(obj):
	data = serializers.serialize('json',[obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data
@csrf_exempt
def unsub(request):
	if request.method == "POST":
		subId = request.POST['id']
		Signup.objects.filter(id=subId).delete()
		return HttpResponse(json.dumps({'message': "Succeed"}),content_type="application/json")
	else:
		return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")
def webservice(request):
	all_data = Member.objects.all().values('nama','email')
	data = list(all_data)
	print(data)
	return JsonResponse(data,safe = False)