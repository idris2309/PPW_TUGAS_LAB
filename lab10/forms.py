from django import forms
from lab10.models import Signup
from django.forms import ModelForm

class Formregister(forms.Form):
	nama = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'id':'post-text1', 'required':True,}), label='Full Name', max_length=50)
     
	email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control', 'id':'post-text2', 'required':True,}), label='E-mail')
    
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','id':'post-text3','required':True,}), min_length=8)
    
	class Meta:
		model =	Signup
		fields = ('nama', 'email', 'password',)

