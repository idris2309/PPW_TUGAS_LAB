from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Signup
from .forms import Formregister

class lab10test(TestCase):

	def test_url_profile_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)
	def test_url_register_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)
	def test_profile_using_profile_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response,'Aboutjss1.html')
	def test_registrasi_using_regis_template(self):
		response = Client().get('/Subscribe/Member')
		self.assertTemplateUsed(response,'Regis.html')
	def test_using_profile_func(self):
		found = resolve('/')
		self.assertEqual(found.func,ProfileS)
	def test_using_regist_func(self):
		found = resolve('/Subscribe/Member')
		self.assertEqual(found.func,Member)

	def test_model_can_add_subscriber(self):
		add_subscriber = Signup.objects.create(nama = "joni",email = "joniganteng@yahoo.com", password = "jonijunior")
		count_obj = Signup.objects.all().count()
		self.assertEqual(count_obj,1)

	def test_add_subscriber(self):
		response = Client().post('/Subscribe/Member',{'nama':"nama",'email':"email",'password':"password"})
		self.assertEqual(response.status_code,302)

	def test_check_email(self):
		response = Client().post('/validate_email/',{'nama':"nama",'email':'email'})
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code,200)
		self.assertJSONEqual(html_response,{'is_taken':False,'is_email_valid':False})
	def test_lab10_succes(self):
		test = 'Anonymous'
		response_post = Client().post('/Subscribe/Member',{'Signup':test})
		self.assertEqual(response_post.status_code,302)
