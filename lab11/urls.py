from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from django.conf import settings
from .views import home


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    url(r'^logout/$', views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
      # <- Here
    url(r'^$', home, name='home'),
    url(r'^databookos/$', views.booklist, name="Book"),
]