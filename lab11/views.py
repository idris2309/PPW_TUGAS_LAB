from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from django.contrib import messages
import urllib.request, json
import requests
from django.views.decorators.csrf import csrf_exempt

def home(request):
	return render(request, 'home.html')
def Book(request):
	return render(request,"Book.html")
@csrf_exempt
def databookos(request):
	user = request.user
	if user is not None:
		jumlahFav = 0
		if request.method == 'POST':
			request.session['jumlahFav'] = request.POST['jumlahFav']

		if request.method != 'POST':

			if user is not None:
				jumlahFav = request.session.get('jumlahFav')
				return render(request, 'Book.html', {'jumlahFav': jumlahFav})
	else :
		return HttpResponseRedirect('/login/')
	return render(request, 'Book.html')

def logout(request):
	request.session.flush()
	return render(request, 'Book.html')

def data(request):
	try:
		q = request.GET['q']
	except:
		q = 'quilting'

	getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q='+ q)
	jsonparse = json.dumps(getJson.json())
	return HttpResponse(jsonparse)

def logout(request):
	request.session.flush()
	return render(request, 'Book.html')
